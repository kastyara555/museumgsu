import exhibits from '../../mock/exhibits.json';

export const getAllExhibits = () => exhibits.items.filter(item => item.show);

export const getExhibits = (searchText = '') => searchText.length > 0 ?
    exhibits.items
        .filter(item => item.show && item.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1)
    : exhibits.items.filter(item => item.show);

export const getRectors = () => exhibits.rectors

export const getPaginatedExhibits = (searchText, page, itemsPerPage) =>
    getExhibits(searchText)
        .filter(item => item.show)
        .slice(itemsPerPage * (page - 1), itemsPerPage * page)

export const getExhibitById = id => [...exhibits.items, ...exhibits.rectors].filter(exhibit => exhibit.show)
    .filter(item => item.id === id)[0]