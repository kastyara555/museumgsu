import React from 'react'
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'

import { defaultImage } from '../../consts/config';

import './ExhibitSearchItem.scss'

const LinkWrapper = ({ link, id, children }) => (
    link ? <a target="_blank" rel="noopener noreferrer" href={link}>{children}</a> : <Link to={`/items/${id}`}>{children}</Link>
)

LinkWrapper.propTypes = {
    children: PropTypes.object,
    link: PropTypes.string,
    id: PropTypes.string
}


const ExhibitSearchItem = ({ item: { id, name, subtitle = null, mainImage = defaultImage, linkTo = null } }) =>
    <LinkWrapper id={id} link={linkTo}>
        <div className="card mb-3 search-item">
            <img alt={mainImage} src={mainImage} className="img-fluid" />
            <div className="card-body">
                <h5 className="card-title">{name}</h5>
                {subtitle && <p className="card-text subtitle">{subtitle}</p>}
            </div>
        </div>
    </LinkWrapper>

ExhibitSearchItem.propTypes = {
    item: PropTypes.object
}

export default ExhibitSearchItem