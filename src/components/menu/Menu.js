import React from 'react'
import { Link } from 'react-router-dom'

import { universeImage } from '../../consts/config'

const Menu = () => (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link className="navbar-brand" to="/">
            <img alt="" src={universeImage} width="96px" />
        </Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
                <Link className="nav-item nav-link" to="/">Главная</Link>
                <div className="dropdown">
                    <Link to="#" className="dropdown-toggle nav-item nav-link" data-toggle="dropdown">Экспозиция <b className="caret"></b></Link>
                    <ul className="dropdown-menu">
                        <li><Link className="nav-item nav-link" to="/rectors">Руководители</Link></li>
                        <li><Link className="nav-item nav-link" to="/exhibits/1">Выставочный зал</Link></li>
                    </ul>
                </div>
                <Link className="nav-item nav-link" to="/about">Справка</Link>
            </div>
        </div>
    </nav>
)

export default Menu