import React from "react";

import logo from "../../images/logo.png";

const About = () => (
  <div className="row">
    <div className="col-lg-2 col-md-2">
      <div className="padded-div">
        <img alt={logo} src={logo} className="img-fluid" />
      </div>
    </div>
    <div className="col-lg-10 col-md-10">
      <div className="padded-div">
        <h1>Музей находится:</h1>
        <p>
          Учреждение образования "Гомельский государственный университет имени
          Франциска Скорины" 246019, г. Гомель, ул.Советская, 104, аудитория
          2-30
        </p>
      </div>
    </div>
  </div>
);

export default About;
