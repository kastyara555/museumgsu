import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types';

import FilterPanel from '../filter-panel/FilterPanel'
import Pagination from '../pagination/Pagination'
import ExhibitsList from '../exhibits-list/ExhibitsList'

import { changePage, setCountOfPages } from '../../redux/actions/pagination'
import history from '../../utils/history'
import { getExhibits, getPaginatedExhibits } from '../../utils/exhibits'
import { changeSearchText } from '../../redux/actions/search'

class Exhibits extends Component {
    constructor(props) {
        super(props);

        this.state = {
            exhibits: [],
            exhibitsOnPage: [],
        };
    }

    componentDidMount() {
        const {
            searchText,
            exhibitsPerPage
        } = this.props;
        const tmpExhibits = getExhibits(searchText);

        var pathArray = window.location.href.split('/');
        this.props.setPage(parseInt(pathArray[pathArray.length - 1]));
        this.props.setCountOfPages(Math.ceil(tmpExhibits.length / exhibitsPerPage));
        this.setState({
            exhibits: tmpExhibits,
            exhibitsOnPage: getPaginatedExhibits(searchText, parseInt(pathArray[pathArray.length - 1]), exhibitsPerPage)
        })
    }

    componentDidUpdate() {
        const {
            exhibitsPerPage
        } = this.props;

        this.props.setCountOfPages(Math.ceil(this.state.exhibits.length / exhibitsPerPage));
    }

    changePage = (page, text) => {
        const {
            exhibitsPerPage
        } = this.props;

        history.push(`/exhibits/${page}`);
        this.props.setPage(page);
        this.setState({
            exhibitsOnPage: getPaginatedExhibits(text, page, exhibitsPerPage),
        });
    }

    changeSearchText = text => {
        this.setState({
            exhibits: getExhibits(text)
        });
        this.props.setSearchText(text);
        this.changePage(1, text);
    }

    render() {
        const {
            searchText,
            totalPages,
            currentPage
        } = this.props;

        const {
            exhibitsOnPage
        } = this.state;

        return (
            <>
                <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href="http://museum.gsu.by/content/install/gsumuseum.apk"
                >
                    <button type="button" class="btn btn-outline-success btn-lg btn-block">
                        Скачать мобильное приложение
                    </button>
                </a>
                <FilterPanel
                    searchText={searchText}
                    changeSearchText={
                        event => this.changeSearchText(event.target.value)
                    }
                />
                <ExhibitsList
                    exhibits={exhibitsOnPage}
                />
                <Pagination
                    total={totalPages}
                    current={currentPage}
                    onPageChange={this.changePage}
                />
            </>
        )
    }
}

Exhibits.propTypes = {
    currentPage: PropTypes.number,
    exhibitsPerPage: PropTypes.number,
    totalPages: PropTypes.number,
    searchText: PropTypes.string,
    setPage: PropTypes.func,
    setCountOfPages: PropTypes.func,
    setSearchText: PropTypes.func
}

const mapStateToProps = state => {
    return ({
        currentPage: state.pagination.page,
        exhibitsPerPage: state.pagination.countPerPage,
        totalPages: state.pagination.countOfPages,
        searchText: state.search
    })
}

const mapDispatchToProps = dispatch => ({
    setPage: page => dispatch(changePage(page)),
    setCountOfPages: count => dispatch(setCountOfPages(count)),
    setSearchText: text => dispatch(changeSearchText(text))
})

export default connect(mapStateToProps, mapDispatchToProps)(Exhibits)