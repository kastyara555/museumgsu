import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import ExhibitSearchItem from '../exhibit-search-item/ExhibitSearchItem';
import { openExhibit } from '../../redux/actions/exhibit';

const ExhibitsList = props => (
    <div className="card">
        <div className="card-header">
            Выставочный зал
        </div>
        <div className="card-body row">
            {
                props.exhibits.map(exhibit => (
                    <div className="col-lg-3 col-md-12" key={exhibit.id} onClick={() => props.openExhibit(exhibit)}>
                        <ExhibitSearchItem item={exhibit} />
                    </div>
                ))
            }
        </div>
    </div>
)

ExhibitsList.propTypes = {
    exhibits: PropTypes.array,
    openExhibit: PropTypes.func
}

const mapDispatchToProps = dispatch => ({
    openExhibit: exhibit => dispatch(openExhibit(exhibit))
})

export default connect(null, mapDispatchToProps)(ExhibitsList);