import React from 'react'

const NoMatchPage = () =>
    <div>
        <h1>Такой страницы не существует...</h1>
    </div>

export default NoMatchPage