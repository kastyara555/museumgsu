import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { closeExhibit, openExhibit } from "../../redux/actions/exhibit";
import { getExhibitById } from "../../utils/exhibits";
import history from "../../utils/history";
import ExhibitSearchItem from "../exhibit-search-item/ExhibitSearchItem";
import Slider from "../slider/Slider";

import "./ExhibitItem.scss";

class ExhibitItem extends React.Component {
  setExhibit = () => {
    let pathArray = window.location.href.split("/");
    let tmpExhibit = getExhibitById(pathArray[pathArray.length - 1]);
    if (!tmpExhibit) {
      return history.push("/404");
    }
    this.props.openExhibit(tmpExhibit);
  };

  componentDidMount() {
    this.setExhibit();
  }

  componentDidUpdate() {
    this.setExhibit();
  }

  componentWillUnmount() {
    this.props.unsetExhibit();
  }

  renderContent = () => {
    let result = [];
    const {
      exhibit: { id, images = [], paragraphs = [] },
    } = this.props;

    for (let i = 0; i < Math.max(images.length, paragraphs.length); i++) {
      result.push([images[i], paragraphs[i]]);
    }

    return (
      <div className="row">
        {result.map(([image, paragraph], index) => (
          <>
            {paragraph && image && index % 2 === 0 && (
              <>
                <div className="col-lg-6 col-md-12 padded-div">
                  {image instanceof Object && !(image instanceof Array) && (
                    <ExhibitSearchItem item={getExhibitById(image.id)} />
                  )}
                  {image instanceof Array && (
                    <Slider id={id.concat(index)} images={image} />
                  )}
                  {!(image instanceof Object) && !(image instanceof Array) && (
                    <img alt={image} src={image} className="img-fluid" />
                  )}
                </div>
                <div className="col-lg-6 col-md-12 padded-div">
                  {paragraph instanceof Array &&
                    paragraph.map((subParagraph) => <p>{subParagraph}</p>)}
                  {!(paragraph instanceof Array) && <p>{paragraph}</p>}
                </div>
              </>
            )}
            {image && paragraph && index % 2 === 1 && (
              <>
                <div className="col-lg-6 col-md-12 padded-div">
                  {paragraph instanceof Array &&
                    paragraph.map((subParagraph) => <p>{subParagraph}</p>)}
                  {!(paragraph instanceof Array) && <p>{paragraph}</p>}
                </div>
                <div className="col-lg-6 col-md-12 padded-div">
                  {image instanceof Object && !(image instanceof Array) && (
                    <ExhibitSearchItem item={getExhibitById(image.id)} />
                  )}
                  {image instanceof Array && (
                    <Slider id={id.concat(index)} images={image} />
                  )}
                  {!(image instanceof Object) && !(image instanceof Array) && (
                    <img alt={image} src={image} className="img-fluid" />
                  )}
                </div>
              </>
            )}
            {!image && paragraph && paragraph instanceof Array && (
              <div className="col-md-12 padded-div">
                {paragraph.map((subParagraph) => (
                  <p>{subParagraph}</p>
                ))}
              </div>
            )}
            {!image && paragraph && !(paragraph instanceof Array) && (
              <div className="col-md-12 padded-div">
                <p>{paragraph}</p>
              </div>
            )}
            {image &&
              !paragraph &&
              image instanceof Object &&
              !(image instanceof Array) && (
                <div className="col-md-12 centered-content padded-div">
                  <ExhibitSearchItem item={getExhibitById(image.id)} />
                </div>
              )}
            {image && !paragraph && image instanceof Array && (
              <div className="col-md-12 centered-content padded-div">
                <Slider id={id.concat(index)} images={image} />
              </div>
            )}
            {image &&
              !paragraph &&
              !(image instanceof Object) &&
              !(image instanceof Array) && (
                <div className="col-md-12 centered-content padded-div">
                  <img alt={image} src={image} className="img-fluid" />
                </div>
              )}
          </>
        ))}
      </div>
    );
  };

  render() {
    const {
      exhibit: { name },
    } = this.props;

    return (
      <>
        <div className="padded-div">
          <h1>{name}</h1>
        </div>
        {this.renderContent()}
      </>
    );
  }
}

ExhibitItem.propTypes = {
  exhibit: PropTypes.object,
  unsetExhibit: PropTypes.func,
  openExhibit: PropTypes.func,
};

const mapStateToProps = (state) => ({
  exhibit: state.exhibit,
});

const mapDispatchToProps = (dispatch) => ({
  unsetExhibit: () => dispatch(closeExhibit()),
  openExhibit: (exhibit) => dispatch(openExhibit(exhibit)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ExhibitItem);
