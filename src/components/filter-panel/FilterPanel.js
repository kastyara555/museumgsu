import React from 'react';
import PropTypes from 'prop-types';

const FilterPanel = ({ searchText, changeSearchText }) => (
    <div className="card">
        <div className="card-header">
            Поиск и фильтрация
        </div>
        <div className="card-body">
            <h6 className="card-subtitle mb-2 text-muted">Введите название:</h6>
            <input type="text" className="form-control" onChange={changeSearchText} value={searchText} />
        </div>
    </div>
)

FilterPanel.propTypes = {
    searchText: PropTypes.string,
    changeSearchText: PropTypes.func,
}

export default FilterPanel