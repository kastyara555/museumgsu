import React from 'react';
import PropTypes from 'prop-types';

class Pagination extends React.Component {
    renderNumbers = () => {
        let numbers = [];
        const { current, total, onPageChange } = this.props;
        for (let i = 1; i < total + 1; i++) {
            numbers.push(
                <li key={i} className={`page-item ${i === current ? 'active' : ''}`}>
                    <button onClick={() => onPageChange(i)} className="page-link">{i}</button>
                </li>
            )
        }
        return numbers
    }

    render()
    {
        const { current, onPageChange, total } = this.props;
        return (
            <>
                {
                    total > 1 &&
                    <nav>
                        <ul className="pagination">
                            <li className={`page-item ${current === 1 ? 'disabled' : ''}`}>
                                <button className="page-link" onClick={() => onPageChange(current - 1)}>Предыдущая</button>
                            </li>
                            {
                                this.renderNumbers()
                            }
                            <li className={`page-item ${current === total ? 'disabled' : ''}`}>
                                <button className="page-link" onClick={() => onPageChange(current + 1)}>Следующая</button>
                            </li>
                        </ul>
                    </nav>
                }
            </>
        )
    }
}

Pagination.propTypes = {
    total: PropTypes.number,
    current: PropTypes.number,
    onPageChange: PropTypes.func,
};

export default Pagination;