import React from 'react';
import PropTypes from 'prop-types';

const Slider = ({ id, images }) =>
    <div id={id} className="carousel slide" data-ride="carousel">
        <ol className="carousel-indicators">
            {
                images.map(
                    (image, index) => <li data-target={`#${id}`} data-slide-to={index} className={`${index === 0 && "active"}`} />
                )
            }
        </ol>
        <div className="carousel-inner">
            {
                images.map(
                    (image, index) => <div className={`carousel-item ${index === 0 && "active"}`}>
                        <img width="100%" height="auto" alt={image} src={image} />
                    </div>
                )
            }
        </div>

        <a className="carousel-control-prev" href={`#${id}`} role="button" data-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
        </a>
        <a className="carousel-control-next" href={`#${id}`} role="button" data-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
        </a>
    </div>

Slider.propTypes = {
    id: PropTypes.string,
    images: PropTypes.array
}

export default Slider