export const OPEN_ITEM = 'OPEN_ITEM';
export const CLOSE_ITEM = 'CLOSE_ITEM';
export const CHANGE_PAGE = 'CHANGE_PAGE';
export const CHANGE_SEARCH_TEXT = 'CHANGE_SEARCH_TEXT';
export const SET_COUNT_OF_PAGES = 'SET_COUNT_OF_PAGES';
