import React from "react";

import "./App.css";
import { Route, Redirect, Switch } from "react-router-dom";
import Exhibits from "./components/exhibits/Exhibits";
import ExhibitItem from "./components/exhibit-item/ExhibitItem";
import About from "./components/about/About";
import MainPage from "./components/main-page/MainPage";
import RectorsList from "./components/rectors-list/RectorsList";
import NoMatchPage from "./components/no-match-page/NoMatchPage";
import { getRectors } from "./utils/exhibits";

const App = () => (
  <Switch>
    <Route exact path="/" component={MainPage} />
    <Route path="/exhibits/:page" component={Exhibits} />
    <Route
      path="/rectors"
      render={() => <RectorsList exhibits={getRectors()} />}
    />
    <Route path="/items/:exhibitId" component={ExhibitItem} />
    <Route path="/about" component={About} />
    <Route path="/404" component={NoMatchPage} />
    <Redirect to="/404" />
  </Switch>
);
export default App;
