import { OPEN_ITEM, CLOSE_ITEM } from '../../consts/actionTypes';

const exhibit = (state = {}, action) => {
    switch (action.type) {
        case OPEN_ITEM:
            return action.payload
        case CLOSE_ITEM:
            return { paragraphs: ['loading'], images: [] }
        default:
            return state
    }
}

export default {
    exhibit
}