import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import exhibit from './exhibitReducer';
import pagination from './paginationReducer';
import search from './searchReducer';

export const reducers = combineReducers(
    Object.assign(
        {},
        {
            ...exhibit,
            ...pagination,
            ...search
        },
        { routing: routerReducer }
    )
);
