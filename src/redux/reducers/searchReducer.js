import { CHANGE_SEARCH_TEXT } from '../../consts/actionTypes';

const search = (state = {}, action) => {
    switch (action.type) {
        case CHANGE_SEARCH_TEXT:
            return action.payload
        default:
            return state
    }
}

export default {
    search
}