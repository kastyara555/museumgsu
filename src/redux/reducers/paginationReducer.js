import { CHANGE_PAGE, SET_COUNT_OF_PAGES } from '../../consts/actionTypes';

const pagination = (state = {}, action) => {
    switch (action.type) {
        case CHANGE_PAGE:
            return {
                ...state,
                page: action.payload
            }
        case SET_COUNT_OF_PAGES:
            return {
                ...state,
                countOfPages: action.payload
            }
        default:
            return state
    }
}

export default {
    pagination
}