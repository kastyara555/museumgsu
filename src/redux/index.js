import { applyMiddleware, compose, createStore } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import ReduxPromise from 'redux-promise';
import thunk from 'redux-thunk';

import history from '../utils/history';
import { reducers } from './reducers'

const composeEnhancers = window.REDUX_DEVTOOLS_EXTENSION_COMPOSE || compose;

export const store = createStore(
    reducers,
    {
        pagination: {
            page: 1,
            countPerPage: 8,
            countOfPages: null
        },
        exhibit: { paragraphs: ['loading'], images: [] },
        search: ''
    },
    composeEnhancers(applyMiddleware(ReduxPromise, thunk))
);

export const synthHistory = syncHistoryWithStore(history, store);