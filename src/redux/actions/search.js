import { CHANGE_SEARCH_TEXT } from '../../consts/actionTypes';

export const changeSearchText = payload => ({
    type: CHANGE_SEARCH_TEXT,
    payload
})