import { CHANGE_PAGE, SET_COUNT_OF_PAGES } from '../../consts/actionTypes';

export const changePage = payload => ({
    type: CHANGE_PAGE,
    payload
})

export const setCountOfPages = payload => ({
    type: SET_COUNT_OF_PAGES,
    payload
})