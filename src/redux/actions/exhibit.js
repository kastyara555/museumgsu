import { OPEN_ITEM, CLOSE_ITEM } from '../../consts/actionTypes';

export const openExhibit = payload => ({
    type: OPEN_ITEM,
    payload
})

export const closeExhibit = payload => ({
    type: CLOSE_ITEM,
    payload
})